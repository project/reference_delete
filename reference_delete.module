<?php
/**
 * @file
 */

/**
 * Implements hook_entity_delete().
 *
 * @todo Add queuing mechanism.
 */
function reference_delete_entity_delete($entity, $type) {
  $entity_id = entity_extract_ids($type, $entity);

  static $queue;
  $queue = empty($queue) ? DrupalQueue::get('reference_delete_delete_references') : $queue;
  $queue->createItem(array(reset($entity_id), $type));
}

/**
 * Calls the hook necessary to delete references to the entity.
 */
function reference_delete_delete_references($entity_id, $type) {
  reference_delete_invoke_all('reference_delete_delete_references', $entity_id, $type);
  field_cache_clear();
}

/**
 * Invokes a hook in all enabled modules that implement it.
 *
 * Replaces module_invoke_all() and bypasses the cache.
 */
function reference_delete_invoke_all($hook) {
  reference_delete_load_includes();

  $args = func_get_args();
  // Remove $hook from the arguments.
  unset($args[0]);
  $return = array();
  foreach (reference_delete_module_implements($hook) as $module) {
    $function = "{$module}_{$hook}";
    if (function_exists($function)) {
      $result = call_user_func_array($function, $args);
      if (isset($result) && is_array($result)) {
        $return = array_merge_recursive($return, $result);
      }
      elseif (isset($result)) {
        $return[] = $result;
      }
    }
  }

  return $return;
}

/**
 * Calls all includes.
 */
function reference_delete_load_includes() {
  $includes = &drupal_static(__FUNCTION__);

  if ( empty($includes) ) {
    foreach ( module_list() as $module ) {
      // Load modules/MODULE.inc files
      if ( !($file = module_load_include('inc', $module, $module . '.reference_delete')) ) {
        // Load supplemental includes only if the module doesn't provide its own implementation
        $includes[$module] = "modules/$module";
      }
    }
  }

  foreach ( $includes as $file ) {
    module_load_include('inc', 'reference_delete', $file);
  }
}

/**
 * Determines which modules are implementing a hook.
 *
 * Replaces module_implements().
 * Does not need to load includes, because they should all have already
 * been loaded in reference_delete_invoke_all().
 */
function reference_delete_module_implements($hook) {
  $implements = &drupal_static(__FUNCTION__);

  if ( empty($implements) ) {
    foreach ( module_list() as $module ) {
      if (function_exists("{$module}_{$hook}")) {
        $implements[] = $module;
      }
    }
  }

  return $implements;
}

/**
 * Returns an array of fields, filtered by module.
 */
function _reference_delete_get_reference_fields($module) {
  $fields = field_info_field_map();

  $reference_fields = array();

  foreach ( $fields as $field_name => &$properties ) {
    $properties = field_info_field($field_name);

    if ($properties['module'] == $module) {
      $reference_fields[$field_name] = $properties;
    }
  }

  if ( !empty($reference_fields) ) {
    return $reference_fields;
  }
}

/**
 * Implements hook_cron_queue_info().
 */
function reference_delete_cron_queue_info() {
  $queues['reference_delete_delete_references'] = array(
    'worker callback' => 'reference_delete_delete_references_queue',
    'time' => 30
  );

  return $queues;
}

/**
 * Queue worker that deletes entity references.
 */
function reference_delete_delete_references_queue($variables) {
  list($entity_id, $type) = $variables;
  reference_delete_delete_references($entity_id, $type);
}
