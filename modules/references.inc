<?php
/**
 * Implements hook_reference_delete_delete_references().
 */
function references_reference_delete_delete_references($entity_id, $type) {
  switch ($type) {
    case 'node' :
      $module = 'node_reference';

      if ( $fields = _references_reference_delete_make_table_names($module) ) {
        foreach ( $fields as $field => $tables ) {
          foreach ( $tables as $table ) {
            db_delete($table)
              ->condition($field . '_nid', $entity_id, '=')
              ->execute();

            watchdog('reference_delete', 'Deleted all references to entity %entity_type #!entity_id from table !table', array('%entity_type' => $type, '!entity_id' => $entity_id, '!table' => $table_name), WATCHDOG_NOTICE);
          }
        }
      }
    break;

    case 'user' :
      $module = 'user_reference';

      if ( $fields = _references_reference_delete_make_table_names($module) ) {
        foreach ( $fields as $field => $tables ) {
          foreach ( $tables as $table_name ) {
            db_delete($table_name)
              ->condition($field . '_uid', $entity_id, '=')
              ->execute();

            watchdog('reference_delete', 'Deleted all references to entity %entity_type #!entity_id from table !table', array('%entity_type' => $type, '!entity_id' => $entity_id, '!table' => $table_name), WATCHDOG_NOTICE);
          }
        }
      }
    break;
  }
}

/**
 * Creates table names (used only for References).
 */
function _references_reference_delete_make_table_names($module) {
  if (isset($module)) {
    $fields = _reference_delete_get_reference_fields($module);

    if ( count($fields) ) {
      $tables = array();
      foreach ( $fields as $field ) {

        $tables[$field] = array('field_data_' . $field, 'field_revision_' . $field);
      }

      return $tables;
    }
  }
}