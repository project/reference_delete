<?php
/**
 * Implements hook_reference_delete_delete_references().
 */
function entityreference_reference_delete_delete_references($entity_id, $type) {
  $module = 'entityreference';

  if ( $fields = _reference_delete_get_reference_fields($module) ) {
    foreach ( $fields as $field_name => $properties ) {
      // Acts only if the target entity is of the same type of the entity that's being deleted
      if ( $properties['settings']['target_type'] == $type ) {
        $tables = reset($properties['storage']['details']);

        foreach ( $tables as $table ) {
          $keys = array_keys($table);
          $table_name = reset($keys);
          $column_name = $table[$table_name]['target_id'];

          db_delete($table_name)
          ->condition($column_name, $entity_id, '=')
          ->execute();

          watchdog('reference_delete', 'Deleted all references to entity %entity_type #!entity_id from table !table', array('%entity_type' => $type, '!entity_id' => $entity_id, '!table' => $table_name), WATCHDOG_NOTICE);
        }
      }
    }
  }
}
